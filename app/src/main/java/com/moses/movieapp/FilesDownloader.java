package com.moses.movieapp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Moshe on 09/10/2016.
 */
public class FilesDownloader {

    public static String downloadTextFile(String stringUrl) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            URL url = new URL(stringUrl);
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String line;
            while ((line = in.readLine()) != null) {
                stringBuilder.append(line);
            }
            in.close();
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        }
        return stringBuilder.toString();
    }
}
