package com.moses.movieapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class MovieDetailsActivity extends AppCompatActivity {

    public static final String EXTRA_MOVIE_ID = "EXTRA_MOVIE_ID";
    private MovieInfo mMovieInfo;
    private TextView mMovieNameTextView;
    private TextView mMovieYearTextView;
    private TextView mMovieCategoryTextView;
    private TextView mMovieDescriptionTextView;
    private TextView mMovieRateTextView;
    private TextView mMovieHourTextView;
    private ImageView mMoviePosterImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        initUIComponents();
        Intent intent = getIntent();
        long movieId = intent.getLongExtra(EXTRA_MOVIE_ID, -1);
        if (movieId == -1) {
            Toast.makeText(getApplicationContext(), " invalid movie id ", Toast.LENGTH_LONG).show();
            this.finish();
        } else {
            new LoadMovieDetailsAsyncTask().execute("http://x-mode.co.il/exam/descriptionMovies/" + movieId + ".txt");
        }
    }

    protected void initUIComponents() {
        mMovieNameTextView = (TextView) findViewById(R.id.movie_name_text_view);
        mMovieYearTextView = (TextView) findViewById(R.id.movie_year_text_view);
        mMovieCategoryTextView = (TextView) findViewById(R.id.movie_category_text_view);
        mMovieDescriptionTextView = (TextView) findViewById(R.id.movie_description_text_view);
        mMovieRateTextView = (TextView) findViewById(R.id.movie_rate_text_view);
        mMovieHourTextView = (TextView) findViewById(R.id.movie_hour_text_view);
        mMoviePosterImageView = (ImageView) findViewById(R.id.movie_poster_image_view);
    }

    protected void setMovieInfoToUIComponents(MovieInfo movieInfo) {
        mMovieInfo = movieInfo;
        if (mMovieInfo != null) {
            mMovieNameTextView.setText(mMovieInfo.getName());
            mMovieYearTextView.setText(mMovieInfo.getYear() + "");
            mMovieCategoryTextView.setText(mMovieInfo.getCategory());
            mMovieDescriptionTextView.setText(mMovieInfo.getDescription());
            mMovieRateTextView.setText(String.valueOf(mMovieInfo.getRate()));
            mMovieHourTextView.setText(mMovieInfo.getHour());
            new DownloadImageAsyncTask(mMoviePosterImageView).execute(mMovieInfo.getImageUrl());
        }
    }

    private class LoadMovieDetailsAsyncTask extends AsyncTask<String, Void, MovieInfo> {

        @Override
        protected MovieInfo doInBackground(String... params) {
            String movieUrl = params[0];
            MovieInfo movieInfo = null;
            String movieJson = FilesDownloader.downloadTextFile(movieUrl);
            JSONObject jsonObj = null;
            try {
                jsonObj = new JSONObject(movieJson);
                movieInfo = MovieInfo.createFromJson(jsonObj);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return movieInfo;
        }

        @Override
        protected void onPostExecute(MovieInfo movieInfo) {
            setMovieInfoToUIComponents(movieInfo);
        }
    }
}
