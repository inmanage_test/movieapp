package com.moses.movieapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MoviesListActivity extends AppCompatActivity {

    private ListView mMoviesListView;
    private List<MovieBasicInfo> mAllMovies;
    private MoviesListAdapter mMoviesListAdapter;
    private EditText mSearchEditText;
    private Button mSearchBtn;
    private Boolean mIsLoadTaskRuning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies_list);
        mMoviesListView = (ListView) findViewById(R.id.movies_list);
        mMoviesListView.setOnItemClickListener(new OnMoviesListItem_Click());
        mSearchEditText = (EditText) findViewById(R.id.movie_search_edit_text);
        mSearchBtn = (Button) findViewById(R.id.search_btn);
        mSearchBtn.setOnClickListener(new OnSearchBtn_Click());
        MoviesListLoaderAsyncTask moviesListLoaderAsyncTask = new MoviesListLoaderAsyncTask();
        moviesListLoaderAsyncTask.execute();
    }


    private class OnMoviesListItem_Click implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(getApplicationContext(), MovieDetailsActivity.class);
            intent.putExtra(MovieDetailsActivity.EXTRA_MOVIE_ID, id);
            startActivity(intent);
        }
    }

    private class OnSearchBtn_Click implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if(mIsLoadTaskRuning) {
                Toast.makeText(getApplicationContext(),"The system loading items to list, Please wait ", Toast.LENGTH_LONG).show();
            }else{
                String searchQ = mSearchEditText.getText().toString();
                new MoviesSearchAsyncTask().execute(searchQ);
            }
        }
    }

    public class MoviesSearchAsyncTask extends AsyncTask<String, Void, List<MovieBasicInfo>> {
        @Override
        protected List<MovieBasicInfo> doInBackground(String... params) {
            String searchKey = params[0];
            List<MovieBasicInfo> moviesListFiltered = new ArrayList<>();
            for(MovieBasicInfo movieBasicInfo : mAllMovies) {
                if(movieBasicInfo.getName().toLowerCase().contains(searchKey.toLowerCase())) {
                    moviesListFiltered.add(movieBasicInfo);
                }
            }
            Collections.sort(moviesListFiltered);
            return moviesListFiltered;
        }

        @Override
        protected void onPreExecute() {
            mIsLoadTaskRuning = true;
        }

        @Override
        protected void onPostExecute(List<MovieBasicInfo> moviesListFiltered) {

            if(mMoviesListAdapter == null) {
                mMoviesListAdapter = new MoviesListAdapter(moviesListFiltered);
                mMoviesListView.setAdapter(mMoviesListAdapter);
            }
            else {
                mMoviesListAdapter.setMoviesList(moviesListFiltered);
            }

            mIsLoadTaskRuning = false;
        }
    }

    public class MoviesListLoaderAsyncTask extends AsyncTask<Void, Void, List<MovieBasicInfo>> {

        @Override
        protected List<MovieBasicInfo> doInBackground(Void... params) {
            String moviesJson = FilesDownloader.downloadTextFile("http://x-mode.co.il/exam/allMovies/allMovies.txt");
//            System.out.println(moviesJson);
            List<MovieBasicInfo> movieBasicInfoList = JsonParseUtils.parseJsonToMoviesBasicInfoCollection(moviesJson);
//            System.out.println("print List: ");
//            System.out.println(movieBasicInfoList);
            Collections.sort(movieBasicInfoList);
            return movieBasicInfoList;
        }

        @Override
        protected void onPreExecute() {
            mIsLoadTaskRuning = true;
        }

        @Override
        protected void onPostExecute(List<MovieBasicInfo> moviesBasicInfo) {
            mAllMovies = moviesBasicInfo;
            if(mMoviesListAdapter == null) {
                mMoviesListAdapter = new MoviesListAdapter(mAllMovies);
                mMoviesListView.setAdapter(mMoviesListAdapter);
            }
            else {
                mMoviesListAdapter.setMoviesList(mAllMovies);
            }

            mIsLoadTaskRuning = false;
        }
    }

    private class MoviesListAdapter extends BaseAdapter {

        private List<MovieBasicInfo> movies;

        public MoviesListAdapter(List<MovieBasicInfo> movies) {
            this.movies = movies;
        }

        public void setMoviesList(List<MovieBasicInfo> moviesToShow) {
            this.movies = moviesToShow;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            if (movies == null)
                return 0;
            else
                return movies.size();
        }

        @Override
        public Object getItem(int position) {
            if (movies == null)
                return null;
            else
                return movies.get(position);
        }

        @Override
        public long getItemId(int position) {
            if (movies == null)
                return 0;
            else
                return movies.get(position).getId();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.movie_list_item, null);
            }

            TextView movieName = (TextView) convertView.findViewById(R.id.movie_name);
            TextView movieYear = (TextView) convertView.findViewById(R.id.movie_year);

            movieName.setText(movies.get(position).getName());
            movieYear.setText(String.valueOf(movies.get(position).getYear()));

            return convertView;
        }
    }
}
