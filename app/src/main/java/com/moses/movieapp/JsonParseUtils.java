package com.moses.movieapp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Moshe on 09/10/2016.
 */
public class JsonParseUtils {

    public static List<MovieBasicInfo> parseJsonToMoviesBasicInfoCollection(String moviesInJson) {
        JSONArray jsonArray = null;
        List<MovieBasicInfo> moviesList = new ArrayList<MovieBasicInfo>();
        try {
            JSONObject jsonObj = new JSONObject(moviesInJson);
            jsonArray = jsonObj.getJSONArray("movies");
            MovieBasicInfo movieToAdd;
            for (int i=0; i<jsonArray.length(); i++) {
                movieToAdd = MovieBasicInfo.createFromJson(jsonArray.getJSONObject(i));
                if(movieToAdd != null) {
                    moviesList.add(movieToAdd);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return moviesList;
    }
}
