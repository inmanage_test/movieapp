package com.moses.movieapp;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Moshe on 09/10/2016.
 */
public class MovieInfo extends MovieBasicInfo {

    private String description;
    private String imageUrl;
    private String promoUrl;
    private int rate;
    private String hour;

    public MovieInfo() {}


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPromoUrl() {
        return promoUrl;
    }

    public void setPromoUrl(String promoUrl) {
        this.promoUrl = promoUrl;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public static MovieInfo createMovieInfo(MovieBasicInfo movieBasicInfo) {
        MovieInfo movieInfo = new MovieInfo();
        movieInfo.setName(movieBasicInfo.getName());
        movieInfo.setYear(movieBasicInfo.getYear());
        movieInfo.setId(movieBasicInfo.getId());
        movieInfo.setCategory(movieBasicInfo.getCategory());

        return movieInfo;
    }

    public static MovieInfo createFromJson(JSONObject jsonObject) throws JSONException {

        MovieInfo movieInfo = null;
        if(jsonObject != null)
        {
            MovieBasicInfo movieBasicInfo = MovieBasicInfo.createFromJson(jsonObject);

            if(movieBasicInfo != null) {
                movieInfo = MovieInfo.createMovieInfo(movieBasicInfo);
            }

            movieInfo.setDescription(jsonObject.getString("description"));
            movieInfo.setImageUrl(jsonObject.getString("imageUrl"));
            movieInfo.setPromoUrl(jsonObject.getString("promoUrl"));
            movieInfo.setHour(jsonObject.getString("hour"));
            movieInfo.setRate(jsonObject.getInt("rate"));
        }

        return movieInfo;
    }
}
