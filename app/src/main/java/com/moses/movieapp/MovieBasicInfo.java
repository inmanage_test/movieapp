package com.moses.movieapp;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Moshe on 09/10/2016.
 */
public class MovieBasicInfo implements Comparable<MovieBasicInfo> {

    private int id;
    private String name;
    private int year;
    private String category;

    public MovieBasicInfo() {}
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public static MovieBasicInfo createFromJson(JSONObject jsonObject) throws JSONException {
        if(jsonObject != null)
        {
            MovieBasicInfo movieBasicInfo = new MovieBasicInfo();
            movieBasicInfo.setId(jsonObject.getInt("id"));
            movieBasicInfo.setName(jsonObject.getString("name"));
            movieBasicInfo.setCategory(jsonObject.getString("category"));
            movieBasicInfo.setYear(jsonObject.getInt("year"));
            return movieBasicInfo;
        }

        return null;
    }

    @Override
    public String toString() {
        return "MovieBasicInfo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", year=" + year +
                ", category='" + category + '\'' +
                '}';
    }


    @Override
    public int compareTo(MovieBasicInfo another) {
        if(this.getYear() > another.getYear()) {
            return 1;
        }else if(this.getYear() < another.getYear()) {
            return -1;
        }else {
            return 0;
        }
    }
}
